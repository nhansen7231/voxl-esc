#!/bin/bash
set -e

for ESC_ID in {0..3}
do
   ./voxl-esc-upload-firmware.py "$@" --id $ESC_ID
done
