# VOXL ESC Tools
VOXL ESC Tools is a Python software package which allows testing, calibration and diagnostics of ESCs using a PC. Note that a compatible ESC from ModalAi is required.

## Hardware Requirements
- Compatible ESC (see [docs](https://docs.modalai.com/modal-escs/) )
- USB-to-serial adapter (3.3V signals) with a 6-pin Hirose DF13 connector (see [docs](https://docs.modalai.com/modal-esc-v2-manual/) for pin-out)
 - the device should appear as /dev/ttyUSBx on Linux machines and /dev/cu.usbserial-* on OSX
- use UART1 pins on the ESC V2 (UART2 is reserved for future use)
- Fixed or adjustable power supply (check ESC data sheet for acceptable voltage), rated for at least several Amps (depending on tests being performed). Note that insufficient current capacity of the power supply may cause ESC reset or other undesired behavior.

## Software Compatibility
- The ESC Tools have been tested using Python 3.6-3.9 (Python 2.7 is no longer supported, use `dev-python27` branch for deprecated version)

## voxl-esc-tools-bin
- `voxl-esc-tools-bin` contains parts of the project which are not open source

## Other Software Prerequisites
### Installation Instructions for Ubuntu 16.04 (and later)

```
#add yourself to group dialout so that you can access serial ports
#(need to log out and log back in after adding to group)
sudo addgroup <your_user_name> dialout

sudo apt-get update
sudo apt-get install -y python3-pip python3-serial python3-numpy

#alternatively, numpy and pyserial can be installed via pip3:
sudo apt-get -y install apt-utils software-properties-common python-pip python-setuptools
pip3 install --upgrade pip3
pip3 install --upgrade numpy pyserial
```

### Building Python from source in Linux (Python 3.8.16 for example)
```
sudo apt-get install build-essential checkinstall
sudo apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev \
    libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev libffi-dev zlib1g-dev

mkdir ~/python-build
cd ~/python-build
wget https://www.python.org/ftp/python/3.8.16/Python-3.8.16.tgz
tar xzf Python-3.8.16.tgz
cd Python-3.8.16
./configure --enable-optimizations
make -j 8
sudo make altinstall
pip3.8 install --upgrade numpy pyserial
```

### Installation Instructions for OSX
```
sudo easy_install pip3
sudo -H pip3 install --upgrade pip3
sudo -H pip3 install --upgrade numpy pyserial
```

### Installation Instructions for VOXL2
- update apt and install python3 packages
```
apt-get update
apt-get install python3-pip python3-serial python3-numpy
```
- install voxl-esc and voxl-esc-slpi-bridge
```
apt-get install voxl-esc voxl-slpi-uart-bridge
```
- enable the slpi bridge and reboot VOXL2
```
voxl-esc enable_bridge
reboot
```
- use `voxl-esc` tools either
   - using `voxl-esc` wrapper script
   - using installed python scripts in `/usr/share/modalai/voxl-esc-tools'
   - your git checkout of the `voxl-esc` repository from `https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-esc` 
 - disable slpi bridge and restore PX4
 ```
 voxl-esc disable_bridge
 reboot
 ```

### (OLD) Installation Instructions for VOXL2
- Make sure to use the latest VOXL2 System Image (tested on 1.5.4-M0054-14.1a-perf-nightly-20230227)
- A bridge library is required to allow CPU to access SLPI UART
   - source code : https://gitlab.com/voxl-public/support/voxl-slpi-uart-bridge
- Step-by-step instructions:

```

#install python3 packages
apt-get install python3-pip python3-serial python3-numpy

#clone the voxl-esc repo
cd /home/root
git clone https://gitlab.com/voxl-public/flight-core-px4/voxl-esc.git -b dev-python3`

#install voxl-esc-tools-bin version 1.1 into voxl-esc/tools directory
#see instructions https://gitlab.com/voxl-public/flight-core-px4/voxl-esc/-/tree/dev-python3/tools#accessing-the-esc-tools-software

#download and install slpi uart bridge package to VOXL2
cd /home/root
wget http://voxl-packages.modalai.com/dists/qrb5165/dev/binary-arm64/voxl-slpi-uart-bridge_1.0.0-202303021121_arm64.deb
dpkg -i voxl-slpi-uart-bridge_1.0.0-202303021121_arm64.deb

#stop and disable PX4 while using voxl-esc tools
systemctl stop voxl-px4 && systemctl disable voxl-px4

#temporarily move libpx4.so and create symlink for slpi bridge to libpx4.so
#this is required because currently the slpi application loader looks for libpx4.so (hardcoded)
cd /usr/lib/rfsa/adsp
mv libpx4.so libpx4.so.bak
ln -s ./libslpi_uart_bridge_slpi.so ./libpx4.so

#reboot VOXL2
reboot

#test voxl-esc tools
cd /home/root/voxl-esc/tools
./voxl-esc-scan.py

#perform ESC testing / calibration
#...

#when done, restore original libpx4.so
cd /usr/lib/rfsa/adsp
rm libpx4.so
cp libpx4.so.bak libpx4.so
systemctl enable voxl-px4
reboot
```

## Usage Examples

### Scanning for ESCs
- the script opens all possible serial ports and scans for ESCs on different baud rates
- if ESC(s) are found, the script receives their ID, Software and Hardware versions
 - Hardware version is board-specific and cannot be changed
 - Firmware version is the version of the ESC Firmware
```
voxl-esc/tools(dev-python3)$ ./voxl-esc-scan.py
Detected Python version : 3.7.12 (default, Dec 27 2021, 16:42:00)
[GCC 5.4.0 20160609]
Found voxl-esc tools bin version: 1.1
INFO: All COM ports:
	/dev/ttyACM1 : STLINK-V3 - ST-Link VCP Ctrl
INFO: UART Port Candidates:
	/dev/ttyACM1
INFO: Scanning for ESC firmware: /dev/ttyACM1, baud: 250000
INFO: ESC(s) detected on port: /dev/ttyACM1, baud rate: 250000
INFO: Detected protocol: firmware
INFO: Additional Information:
INFO: ---------------------
	ID         : 0
	Board      : version 31: ModalAi 4-in-1 ESC V2 RevB (M0049)
	UID        : 0x203334334841570D00250044
	Firmware   : version   34, hash 833243c0
	Bootloader : version  183, hash 0a80a32a

	ID         : 1
	Board      : version 31: ModalAi 4-in-1 ESC V2 RevB (M0049)
	UID        : 0x203334334841570D00360044
	Firmware   : version   36, hash 174d53e1*
	Bootloader : version  183, hash 0a80a32a

	ID         : 2
	Board      : version 31: ModalAi 4-in-1 ESC V2 RevB (M0049)
	UID        : 0x203334334841570D00210045
	Firmware   : version   34, hash 833243c0
	Bootloader : version  183, hash 0a80a32a

	ID         : 3
	Board      : version 31: ModalAi 4-in-1 ESC V2 RevB (M0049)
	UID        : 0x203334334841570D001F0045
	Firmware   : version   34, hash 833243c0
	Bootloader : version  183, hash 0a80a32a

---------------------
```

### Scanning for ESCs on VOXL2
```
voxl2:~/voxl-esc/tools(dev-python3)$ ./voxl-esc-scan.py
Detected Python version : 3.6.9 (default, Nov 25 2022, 14:10:45)
[GCC 8.4.0]
Found voxl-esc tools bin version: 1.1
VOXL Platform: M0054
Detected VOXL2 M0054 or M0104!
Found previous connection information in .voxl_esc_cache ..
Prioritizing /dev/slpi-uart-2 @ 250000
INFO: Scanning for ESC firmware: /dev/slpi-uart-2, baud: 250000
INFO: ESC(s) detected on port: /dev/slpi-uart-2, baud rate: 250000
INFO: Detected protocol: firmware
INFO: Additional Information:
INFO: ---------------------
	ID         : 0
	Board      : version 36: ModalAi 4-in-1 ESC V2 RevC-3 (M0117-3)
	UID        : 0x203034305743570C0031004C
	Firmware   : version   36, hash 9db99d7c*
	Bootloader : version  183, hash 12719b2c

	ID         : 1
	Board      : version 36: ModalAi 4-in-1 ESC V2 RevC-3 (M0117-3)
	UID        : 0x203034305743570C0034002E
	Firmware   : version   36, hash 49f19da6
	Bootloader : version  183, hash 12719b2c

	ID         : 2
	Board      : version 36: ModalAi 4-in-1 ESC V2 RevC-3 (M0117-3)
	UID        : 0x203034305743570C0034004A
	Firmware   : version   36, hash 49f19da6*
	Bootloader : version  183, hash 12719b2c

	ID         : 3
	Board      : version 36: ModalAi 4-in-1 ESC V2 RevC-3 (M0117-3)
	UID        : 0x203034305743570C00340046
	Firmware   : version   36, hash 49f19da6
	Bootloader : version  183, hash 12719b2c

---------------------
```

### Uploading Parameters
```
python voxl-esc-upload-params.py --params-file ../params/esc_params_modalai_4_in_1.xml
Scanning for ESC firmware: /dev/cu.usbserial-A7027C8E, baud: 250000

ESC(s) detected on port: /dev/cu.usbserial-A7027C8E using baudrate: 250000
Attempting to open...
ESCs detected:
---------------------
ID: 0, SW: 31, HW: 30
ID: 1, SW: 31, HW: 30
ID: 2, SW: 31, HW: 30
ID: 3, SW: 31, HW: 30
---------------------
Loading XML config file...
Uploading params...
-- board config
-- id config
-- uart config
-- tune config
    DONE
Resetting...
    DONE
```

### Spinning Motors
- if ID 255 is specified, all detected ESCs will be commanded to spin, otherwise just the single specified ID
- be very careful when specifying desired power or rpm (start with low power like 10, if unsure)
- you will be prompted to type "yes" before motors will spin (for safety)
```
python voxl-esc-spin.py --id 0 --power 10
python voxl-esc-spin.py --id 255 --power 10
python voxl-esc-spin.py --id 0 --rpm 3000
```

```
python voxl-esc-spin.py --id 255 --power 10
Scanning for ESC firmware: /dev/cu.usbserial-A7027C8E, baud: 250000

ESC(s) detected on port: /dev/cu.usbserial-A7027C8E using baudrate: 250000
Attempting to open...
WARNING:
This test requires motors to spin at high speeds with
propellers attached. Please ensure that appropriate
protective equipment is being worn at all times and
that the motor and propeller are adequately isolated
from all persons.

For best results, please perform this test at the
nominal voltage for the battery used.

Type "Yes" to continue: yes
[0] RPM: 0, PWR: 0, VBAT: 12.13V, TEMPERATURE: 39.09C, CURRENT: 0.02A
[1] RPM: 0, PWR: 0, VBAT: 12.16V, TEMPERATURE: 39.89C, CURRENT: 0.00A
[2] RPM: 0, PWR: 0, VBAT: 12.18V, TEMPERATURE: 39.81C, CURRENT: 0.01A
[3] RPM: 0, PWR: 0, VBAT: 12.18V, TEMPERATURE: 39.54C, CURRENT: 0.00A
[0] RPM: 0, PWR: 0, VBAT: 12.13V, TEMPERATURE: 39.09C, CURRENT: 0.02A
[1] RPM: 0, PWR: 0, VBAT: 12.16V, TEMPERATURE: 39.89C, CURRENT: 0.00A
[2] RPM: 0, PWR: 0, VBAT: 12.18V, TEMPERATURE: 39.81C, CURRENT: 0.01A
[3] RPM: 0, PWR: 0, VBAT: 12.18V, TEMPERATURE: 39.54C, CURRENT: 0.00A
...
...
[0] RPM: 5229, PWR: 10, VBAT: 12.12V, TEMPERATURE: 39.03C, CURRENT: 0.06A
[1] RPM: 5226, PWR: 10, VBAT: 12.14V, TEMPERATURE: 39.80C, CURRENT: 0.06A
[2] RPM: 5242, PWR: 10, VBAT: 12.15V, TEMPERATURE: 39.70C, CURRENT: 0.10A
[3] RPM: 5279, PWR: 10, VBAT: 12.15V, TEMPERATURE: 39.31C, CURRENT: 0.05A
[0] RPM: 5239, PWR: 10, VBAT: 12.11V, TEMPERATURE: 38.94C, CURRENT: 0.03A
[1] RPM: 5226, PWR: 10, VBAT: 12.14V, TEMPERATURE: 39.80C, CURRENT: 0.06A
[2] RPM: 5242, PWR: 10, VBAT: 12.15V, TEMPERATURE: 39.70C, CURRENT: 0.10A
[3] RPM: 5277, PWR: 10, VBAT: 12.15V, TEMPERATURE: 39.37C, CURRENT: 0.11A
...
```

## ESC Parameters

Some ESC parameter examples are maintained in this repository in *params* directory. See the XML parameter files for details and additional documentation
