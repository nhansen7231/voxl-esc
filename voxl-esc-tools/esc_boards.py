def get_esc_board_description(hardware_version):
    hardware_name = 'Unknown Board'
    if hardware_version == 30:
        hardware_name = 'ModalAi 4-in-1 ESC V2 RevA'
    elif hardware_version == 31:
        hardware_name = 'ModalAi 4-in-1 ESC V2 RevB (M0049)'
    elif hardware_version == 32:
        hardware_name = 'Blheli32 4-in-1 ESC Type A (Tmotor F55A PRO F051)'
    elif hardware_version == 33:
        hardware_name = 'Blheli32 4-in-1 ESC Type B (Tmotor F55A PRO G071)'
    elif hardware_version == 34:
        hardware_name = 'ModalAi 4-in-1 ESC (M0117-1)'
    elif hardware_version == 35:
        hardware_name = 'ModalAi M0065 PX4IO (M0065)'
    elif hardware_version == 36:
        hardware_name = 'ModalAi 4-in-1 ESC (M0117-3)'
    elif hardware_version == 37:
        hardware_name = 'ModalAi 4-in-1 ESC (M0134-1)'
    elif hardware_version == 38:
        hardware_name = 'ModalAi 4-in-1 ESC (M0134-3)'
    elif hardware_version == 39:
        hardware_name = 'ModalAi 4-in-1 ESC (M0129-1)'
    elif hardware_version == 40:
        hardware_name = 'ModalAi 4-in-1 ESC (M0129-3)'
    elif hardware_version == 41:
        hardware_name = 'ModalAi 4-in-1 ESC (M0134-6)'

    return hardware_name
